package de.htw_berlin.gma.projektmanagement;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

/**
 * Diese Klasse ist für das Bearbeiten und Speichern von Aufgaben zuständig
 * @author Delia Metzler
 * @author Igor Turanin
 */
public class AufgabeBearbeiten extends AppCompatActivity {

    // Konstante für die Fallunterscheidung: Neue Aufgabe oder Aufgabe bearbeiten
    final static int NEUE_AUFGABE = -1;

    // Variable für die Fallunterscheidung: Neue Aufgabe oder Aufgabe bearbeiten
    // wird in onCreate() initialisiert
    private int a_id;

    // Deklaration notwendiger Variablen //

    // ermöglicht nach späterer Initialisierung den Zugriff auf die lokale Datenbank der App
    DB datenbank;

    // ermöglichen nach späterer Initialisierung
    // den Zugriff auf die Eingabefelder und das Dropdown-Menü
    EditText bezeichnung;
    EditText beschreibung;
    EditText deadline;
    EditText bearbeiter;
    Spinner status;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_aufgabe_bearbeiten);

        //Zurück-Pfeil auf dem Actionbar erzeugen
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // neues Objekt der Klasse DB wird erstellt um den Zugriff
        // auf die lokale Datenbank zu gewährleisten
        datenbank = new DB(getApplicationContext());

        // hier wird die Aufgaben-ID abgespeichert
        // wenn kein Extra mit dem Tag "a_id" übergeben wurde,
        // wird standardmäßig -1 zugewiesen
        a_id = getIntent().getIntExtra("a_id", NEUE_AUFGABE);

        // Informationen einer Aufgabe werden über die Aufgaben-ID
        // abgerufen und in einem String-Array gespeichert
        String[] aufgabe = datenbank.queryAufgabe(a_id);

        // Zuweisung der Eingabefelder zu den Variablen
        bezeichnung = (EditText) findViewById(R.id.ed_bezeichnung_aufgabe);
        beschreibung = (EditText) findViewById(R.id.ed_beschreibung_aufgabe);
        deadline = (EditText) findViewById(R.id.ed_deadline);
        bearbeiter = (EditText) findViewById(R.id.ed_bearbeiter);
        status = (Spinner) findViewById(R.id.dd_status);

        // die Aufgabeninformationen werden in die Eingabefelder
        // geschrieben, damit der Nutzer diese Bearbeiten kann
        bezeichnung.setText(aufgabe[1]);
        beschreibung.setText(aufgabe[2]);
        deadline.setText(aufgabe[3]);
        bearbeiter.setText(aufgabe[4]);

        // wenn a_id einer tatsächlichen Aufgaben-ID entspricht,
        // dann wird die Auswahl des Dropdown-Menüs auf den aktuellen Status
        // der Aufgabe gesetzt

        // Zusätzlich wird der Titel der Activity angepasst
        // Standard-Titel: "Neue Aufgabe"
        if (a_id != NEUE_AUFGABE) {
            status.setSelection(Integer.parseInt(aufgabe[5]));
            setTitle(getString(R.string.title_activity_aufgabe_bearbeiten));
        }

    }

    /**
     * onClick-Methode für die beiden Buttons der Activity
     * @param v View-Objekt des betätigten Buttons
     */
    public void onButtonClick(View v) {

        // hier wird der Status der Aufgabe aus dem Dropdown-Menü ausgelesen
        // der Status entspricht den Positionen der Items im Menü
        // * 0 - Offen
        // * 1 - In Bearbeitung
        // * 2 - Abgeschlossen
        int st = status.getSelectedItemPosition();

        // beim Klicken auf den Abbrechen-Button wird die Activity einfach geschlossen
        if (v.getId() == R.id.btn_abbrechen_aufgabe) {
            finish();
        } else if (v.getId() == R.id.btn_speichern_aufgabe) {

            // beim Klicken auf den Speichern-Button wird zuerst geprüft
            // ob in den Eingabefeldern etwas steht.
            // Wenn in einem Feld nichts steht, dann wird ein Standard-Text gesetzt
            if (bezeichnung.getText().toString().trim().equals("")) {
                bezeichnung.setText(getString(R.string.str_keine_Bezeichnung));
            }

            if (beschreibung.getText().toString().trim().equals("")) {
                beschreibung.setText(getString(R.string.str_keine_Beschreibung));
            }

            if (deadline.getText().toString().trim().equals("")) {
                deadline.setText(getString(R.string.str_keine_Deadline));
            }

            if (bearbeiter.getText().toString().trim().equals("")) {
                bearbeiter.setText(getString(R.string.str_kein_Bearbeiter));
            }

            // hier wird geprüft ob es sich um eine neue oder eine bestehende Aufgabe handelt
            if(a_id == NEUE_AUFGABE) {

                // wenn ja, dann wird eine neue Aufgabe mit passender Methode
                // in der Datenbank erstellt
                datenbank.neueAufgabe(bezeichnung.getText().toString(), beschreibung.getText().toString(),
                        deadline.getText().toString(), bearbeiter.getText().toString(), st);

            } else {

                // sonst wird die bestehende Aufgabe in der Datenbank bearbeitet
                datenbank.bearbeitenAufgabe(a_id, bezeichnung.getText().toString(), beschreibung.getText().toString(),
                        deadline.getText().toString(), bearbeiter.getText().toString(), st);
            }

            // das Resultat der Activity wird auf OK gesetzt
            // um der Activity "Aufgabe" mitzuteilen, dass alles funktioniert hat
            // danach wird die bestehende Activity geschlossen
            setResult(RESULT_OK);
            finish();

        }
    }

    /**
     * onOptionsItemSelected-Methode für den Zurück-Button auf dem Action-Bar
     * @param item entsprechendes Menü-Item
     * @return boolean Ausführen
     */
    public boolean onOptionsItemSelected(MenuItem item) {
        //zur vorherigen Activity wechseln
        onBackPressed();
        return true;
    }
}
