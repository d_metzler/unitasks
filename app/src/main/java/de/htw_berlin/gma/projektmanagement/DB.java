package de.htw_berlin.gma.projektmanagement;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;

/**
 * Diese Klasse ist für den Zugriff auf die lokale Datenbank zuständig
 * @author Delia Metzler
 * @author Igor Turanin
 */
public class DB extends SQLiteOpenHelper {

    // Datenbankname und Version
    private static final String DATABASE_NAME = "projekte.db";
    private static final int DATABASE_VERSION = 1;

    // Aufgaben-Tabelle
    // Hier werden Tabellenname und Spaltennamen festgelegt
    private static final String AUFGABEN = "aufgabe";
    private static final String AUFGABEN_ID = "a_id";
    private static final String AUFGABEN_BEZEICHNUNG = "a_bezeichnung";
    private static final String AUFGABEN_BESCHREIBUNG = "a_beschreibung";
    private static final String AUFGABEN_DEADLINE = "a_deadline";
    private static final String AUFGABEN_BEARBEITER = "a_bearbeiter";
    private static final String AUFGABEN_STATUS = "a_status";

    // Mögliche Werte für AUFGABEN_STATUS:
    // * 0 - Offen
    // * 1 - In Bearbeitung
    // * 2 - Abgeschlossen

    // Konstante zum Prüfen ob die Abfrage beim erstellen einer neuen Aufgabe erfolgt
    private final static int NEUE_AUFGABE = -1;

    /**
     * Konstruktor für die Klasse.
     * Legt fest auf welche lokale Datenbank zugegriffen wird.
     * @param context
     */
    public DB(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    /**
     * In dieser Methode wird eine neue Tabelle in der Datenbank erstellt,
     * wenn noch keine vorhanden ist.
     * @param db
     */
    @Override
    public void onCreate(SQLiteDatabase db) {
        String sql = "CREATE TABLE " + AUFGABEN + " ( " + AUFGABEN_ID + " INTEGER PRIMARY KEY, "
                + AUFGABEN_BEZEICHNUNG + " VARCHAR(30), " + AUFGABEN_BESCHREIBUNG + " TEXT, "
                + AUFGABEN_DEADLINE + " VARCHAR(10), " + AUFGABEN_BEARBEITER + " VARCHAR(40), "
                + AUFGABEN_STATUS + " INTEGER );";
        db.execSQL(sql);
    }

    /**
     * Diese Methode bestimmt was passiert, wenn eine Datenbank mit höherer Version
     * bereitsteht.
     * @param db
     * @param oldVersion
     * @param newVersion
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        // wenn eine neue Version existiert, wird die alte Tabelle gelöscht
        // und eine neue Datenbank erstellt
        if(oldVersion != newVersion) {
            db.execSQL("DROP TABLE IF EXISTS " + AUFGABEN);
            onCreate(db);
        }
    }

    /**
     * Diese Methode speichert alle Aufgaben-Bezeichnungen mit passendem Statuseintrag.
     * @param dd_position Index im Statusfilter-Dropdown-Menü
     * @return Liste mit Aufgaben-Bezeichnungen
     */
    public ArrayList<String> getBezeichnungen(int dd_position) {

        // Hier wird der erste Teil der SQL-Anfrage gebildet.
        // Wir wählen alle Bezeichnungen in der Tabelle "aufgaben"
        String sql = "SELECT " + AUFGABEN_BEZEICHNUNG + " FROM " + AUFGABEN;

        // hier wird die SQL-Anfrage je nach Status vervollständigt
        switch(dd_position){
            case 0: break;                                                     // kein Filter
            case 1: sql = sql + " WHERE " + AUFGABEN_STATUS + "=" + 0; break;  // offen
            case 2: sql = sql + " WHERE " + AUFGABEN_STATUS + "=" + 1; break;  // in Bearbeitung
            case 3: sql = sql + " WHERE " + AUFGABEN_STATUS + "=" + 2; break;  // Abgeschlossen
            default: break;
        }

        // danach beschaffen wir uns Leserechte für die lokale Datenbank
        // und führen unsere SQL-Abfrage durch
        SQLiteDatabase db = getReadableDatabase();

        // rawQuery() gibt einen Cursor zurück mit dem wir durch das Ergebnis navigieren können
        Cursor cursor = db.rawQuery(sql, null);

        // hier wird eine Liste erstellt, in der die Bezeichnungen gespeichert werden sollen
        ArrayList<String> erg = new ArrayList<>();

        // dann laufen wir das Ergebnis der SQL-Abfrage ab und speichern die Bezeichnungen
        if (cursor.moveToFirst()) {

            do {
                erg.add(cursor.getString(cursor.getColumnIndex(AUFGABEN_BEZEICHNUNG)));
            } while (cursor.moveToNext());
        }

        // danach schließen wir den Cursor und den Zugriff auf die Datenbank
        cursor.close();
        db.close();

        // Zum Schluss wird die Liste zurückgegeben
        return erg;
    }

    /**
     * Diese Methode speichert alle Aufgaben-IDs mit passendem Statuseintrag.
     * @param dd_position Index im Statusfilter-Dropdown-Menü
     * @return Liste mit Aufgaben-IDs
     */
    public ArrayList<Integer> getAufgabenIDs(int dd_position) {

        // Hier wird der erste Teil der SQL-Anfrage gebildet.
        // Wir wählen alle IDs in der Tabelle "aufgaben"
        String sql = "SELECT " + AUFGABEN_ID + " FROM " + AUFGABEN;

        // hier wird die SQL-Anfrage je nach Status vervollständigt
        switch(dd_position){
            case 0: break;                                                     // kein Filter
            case 1: sql = sql + " WHERE " + AUFGABEN_STATUS + "=" + 0; break;  // offen
            case 2: sql = sql + " WHERE " + AUFGABEN_STATUS + "=" + 1; break;  // in Bearbeitung
            case 3: sql = sql + " WHERE " + AUFGABEN_STATUS + "=" + 2; break;  // Abgeschlossen
            default: break;
        }

        // danach beschaffen wir uns Leserechte für die lokale Datenbank
        // und führen unsere SQL-Abfrage durch
        SQLiteDatabase db = getReadableDatabase();

        // rawQuery() gibt einen Cursor zurück mit dem wir durch das Ergebnis navigieren können
        Cursor cursor = db.rawQuery(sql, null);

        // hier wird eine Liste erstellt, in der die IDs gespeichert werden sollen
        ArrayList<Integer> erg = new ArrayList<>();

        // dann laufen wir das Ergebnis der SQL-Abfrage ab und speichern die IDs
        if (cursor.moveToFirst()) {

            do {
                erg.add(cursor.getInt(cursor.getColumnIndex(AUFGABEN_ID)));
            } while (cursor.moveToNext());
        }

        // danach schließen wir den Cursor und den Zugriff auf die Datenbank
        cursor.close();
        db.close();

        // Zum Schluss wird die Liste zurückgegeben
        return erg;
    }

    /**
     * Diese Methode beschafft alle Informationen einer geforderten Aufgabe.
     * @param id Die ID der geforderten Aufgabe
     * @return String-Array mit den Attributen der Aufgabe
     */
    public String[] queryAufgabe(int id) {

        // Wir beschaffen uns Leserechte für die lokale Datenbank
        SQLiteDatabase db = getReadableDatabase();

        // Die SQL-Abfrage:
        // Wir suchen alle Attribute eines Eintrags in der Tabelle "aufgaben"
        // in welchem die gesuchte ID gespeichert ist
        String sql = "SELECT * FROM " + AUFGABEN + " WHERE " + AUFGABEN_ID + "=" + id;

        // rawQuery() gibt einen Cursor zurück mit dem wir durch das Ergebnis navigieren können
        Cursor cursor = db.rawQuery(sql, null);

        // in diesem Array werden die Attribute gespeichert
        // Index:
        // 0 - ID
        // 1 - Bezeichnung
        // 2 - Beschreibung
        // 3 - Deadline
        // 4 - Bearbeiter
        // 5 - Status
        String[] erg = new String[6];

        // Wir laufen das Ergebnis der SQL-Abfrage ab und speichern die Attribute.
        // Wenn "id" einer neuen Aufgabe entspricht, wird der Schritt übersprungen
        // und am Ende ein leeres Array zurückgegeben
        if(cursor.moveToFirst() && id != NEUE_AUFGABE) {

            erg[0] = cursor.getString(cursor.getColumnIndex(AUFGABEN_ID));
            erg[1] = cursor.getString(cursor.getColumnIndex(AUFGABEN_BEZEICHNUNG));
            erg[2] = cursor.getString(cursor.getColumnIndex(AUFGABEN_BESCHREIBUNG));
            erg[3] = cursor.getString(cursor.getColumnIndex(AUFGABEN_DEADLINE));
            erg[4] = cursor.getString(cursor.getColumnIndex(AUFGABEN_BEARBEITER));
            erg[5] = cursor.getString(cursor.getColumnIndex(AUFGABEN_STATUS));
        }

        // danach schließen wir den Cursor und den Zugriff auf die Datenbank
        cursor.close();
        db.close();

        // das Ergebnis wird anschließend zurückgegeben
        return erg;
    }

    /**
     * Diese Methode erstellt eine neue Aufgabe mit übergebenen Informationen.
     * @param bz Aufgaben-Bezeichnung
     * @param bs Aufgaben-Beschreibung
     * @param dl Aufgaben-Deadline
     * @param ba Aufgaben-Bearbeiter
     * @param st Aufgaben-Status
     */
    public void neueAufgabe(String bz, String bs, String dl, String ba, int st) {

        // Wir beschaffen uns Schreibrechte für die lokale Datenbank
        SQLiteDatabase db = getWritableDatabase();

        // hier werden die Werte der Spalten des neuen Eintrags gespeichert
        ContentValues values = new ContentValues();
        values.put(AUFGABEN_BEZEICHNUNG, bz);
        values.put(AUFGABEN_BESCHREIBUNG, bs);
        values.put(AUFGABEN_DEADLINE, dl);
        values.put(AUFGABEN_BEARBEITER, ba);
        values.put(AUFGABEN_STATUS, st);

        // anschließend werden die Werte in die Tabelle "aufgaben" eingefügt
        // und der Schreibzugriff geschlossen
        db.insert(AUFGABEN, null, values);
        db.close();
    }

    /**
     * Diese Methode ändert Informationen einer Aufgabe.
     * @param id ID der zu bearbeitenden Aufgabe
     * @param bz Aufgaben-Bezeichnung
     * @param bs Aufgaben-Beschreibung
     * @param dl Aufgaben-Deadline
     * @param ba Aufgaben-Bearbeiter
     * @param st Aufgaben-Status
     */
    public void bearbeitenAufgabe(int id ,String bz, String bs, String dl, String ba, int st) {

        // Wir beschaffen uns Schreibrechte für die lokale Datenbank
        SQLiteDatabase db = getWritableDatabase();

        // hier werden die neuen Werte der Spalten des Eintrags gespeichert
        ContentValues values = new ContentValues();
        values.put(AUFGABEN_BEZEICHNUNG, bz);
        values.put(AUFGABEN_BESCHREIBUNG, bs);
        values.put(AUFGABEN_DEADLINE, dl);
        values.put(AUFGABEN_BEARBEITER, ba);
        values.put(AUFGABEN_STATUS, st);

        // anschließend werden die Werte des Eintrags mit der passenden ID
        // mit neuen Werten überschrieben und der Schreibzugriff wird geschlossen
        db.update(AUFGABEN, values, AUFGABEN_ID + "=" + id, null);
        db.close();
    }

    /**
     * Diese Methode löscht eine Aufgabe.
     * @param id ID der zu löschenden Aufgabe
     */
    public void deleteAufgabe(int id) {

        // Wir beschaffen uns Schreibrechte für die lokale Datenbank
        SQLiteDatabase db = getWritableDatabase();

        // Wir löschen eine Aufgabe mit der passenden ID aus der Tabelle "aufgaben"
        // und schließen den Schreibzugriff
        db.delete(AUFGABEN, AUFGABEN_ID + "=" + id, null);
        db.close();
    }

    public String[] queryStatus() {

        // Wir beschaffen uns Leserechte für die lokale Datenbank
        SQLiteDatabase db = getReadableDatabase();

        // Die SQL-Abfrage:
        String sql = "SELECT COUNT(" + AUFGABEN_STATUS + ") FROM " + AUFGABEN + " WHERE " + AUFGABEN_STATUS + "=0";
        String sql1 = "SELECT COUNT(" + AUFGABEN_STATUS + ") FROM " + AUFGABEN + " WHERE " + AUFGABEN_STATUS + "=1";
        String sql2 = "SELECT COUNT(" + AUFGABEN_STATUS + ") FROM " + AUFGABEN + " WHERE " + AUFGABEN_STATUS + "=2";

        // rawQuery() gibt einen Cursor zurück mit dem wir durch das Ergebnis navigieren können
        Cursor cursor = db.rawQuery(sql, null);
        Cursor cursor1 = db.rawQuery(sql1, null);
        Cursor cursor2 = db.rawQuery(sql2, null);

        String[] erg = new String[3];
        int i = 0;
        cursor.moveToFirst();
        erg[i] = cursor.getString(0);
        i++;
        cursor.close();
        cursor1.moveToFirst();
        erg[i] = cursor1.getString(0);
        i++;
        cursor1.close();
        cursor2.moveToFirst();
        erg[i] = cursor2.getString(0);
        i++;
        cursor2.close();
        db.close();

        // das Ergebnis wird anschließend zurückgegeben
        return erg;
    }

}
