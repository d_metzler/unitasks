package de.htw_berlin.gma.projektmanagement;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Diese Klasse zeigt die Übersicht über die abgespeicherten Aufgaben.
 * zugehörige Activity: activity_aufgaben_uebersicht.xml
 * @author Delia Metzler
 * @author Igor Turanin
 */
public class AufgabenUebersicht extends AppCompatActivity implements AdapterView.OnItemClickListener, AdapterView.OnItemLongClickListener, AdapterView.OnItemSelectedListener{

    // Deklaration notwendiger Variablen //

    // ermöglicht nach späterer Initialisierung den Zugriff auf die lokale Datenbank der App
    DB datenbank;

    ListView aufgaben;
    ArrayAdapter<String> listAdapter = null;                //zum Zugreifen auf die ListView
    ArrayList<String> bezeichnungen = new ArrayList<>();    //StringArray-Listen zum befüllen der Listen
    ArrayList<Integer> ids = new ArrayList<>();
    int aID = -1;
    Spinner statusfilter;
    BarchartView barchart;
    TextView gesamteAufgaben;

    final static int AUFGABE_ANZEIGEN = 10;                 //Request-Codes
    final static int NEUE_AUFGABE = 9;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_aufgaben_uebersicht);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setTitle(getString(R.string.title_activity_aufgaben_uebersicht));

        // neues Objekt der Klasse DB wird erstellt um den Zugriff
        // auf die lokale Datenbank zu gewährleisten
        datenbank = new DB(getApplicationContext());

        //Deklaration und Initialisierung von benötigten Objekten, Adaptern und Listenern
        statusfilter = (Spinner) findViewById(R.id.dd_statusfilter);
        gesamteAufgaben = (TextView) findViewById(R.id.tv_aufgaben_gesamt);
        aufgaben = (ListView) findViewById(R.id.lv_aufgaben);
        aufgaben.setAdapter(listAdapter);
        aufgaben.setOnItemClickListener(this);
        aufgaben.setOnItemLongClickListener(this);
        statusfilter.setOnItemSelectedListener(this);

        //Inhalt der Activity erzeugen
        onCreateInhalt();

        //Button, um neue Aufgabe zu erstellen
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), AufgabeBearbeiten.class);   //Activity von AufgabeBearbeiten.class starten
                startActivityForResult(intent, NEUE_AUFGABE);                                   //Requestcode-Übergabe zur Aktionsunterscheidung
            }
        });
    }



    /**
     * onItemClick-Methode für die ListView und deren einzelne Items
     * @param parent AdapterView
     * @param view View-Objekt des ausgewählten Items
     * @param position Position des ausgewählten Listenelements als Integer
     * @param id ID des ausgewählten Listenelemente
     */
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        //Aufgaben-ID ermitteln durch Zugriff auf ID-Liste
        aID = ids.get(position);

        //neue Activity starten
        Intent intent = new Intent(this, Aufgabe.class);
        intent.putExtra("a_id", aID);                       //Zusatzinformationen für die nächste Activity
        startActivityForResult(intent, AUFGABE_ANZEIGEN);   //Requestcode-Übergabe zur Aktionsunterscheidung
    }


    /**
     * onItemClick-Methode für die ListView und deren einzelne Items
     * @param parent AdapterView
     * @param view View-Objekt des ausgewählten Items
     * @param position Position des ausgewählten Listenelements als Integer
     * @param id ID des ausgewählten Listenelemente
     */
    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {

        //Methode loeschenDialog starten -> Pop-Up öffnen
        loeschenDialog(position);

        // führt onItemClick() nicht aus
        return true;
    }

    /**
     * Inhalte der Activity erzeugen
     */
    private void onCreateInhalt() {
        //Array-Listen mit Aufgabenbezeichnungen und -IDs füllen, je nachdem welcher Filter ausgewählt ist
        bezeichnungen = datenbank.getBezeichnungen(statusfilter.getSelectedItemPosition());
        ids = datenbank.getAufgabenIDs(statusfilter.getSelectedItemPosition());

        //ListView mit Aufgabenbezeichnungen füllen
        listAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1,bezeichnungen);
        //Listen-Adapter setzen
        aufgaben.setAdapter(listAdapter);

        String[] status = datenbank.queryStatus();
        int aufgabenanzahl = Integer.parseInt(status[0]) + Integer.parseInt(status[1]) + Integer.parseInt(status[2]);
        gesamteAufgaben.setText(getString(R.string.str_gesamt) + " " + aufgabenanzahl);

        RelativeLayout layout = (RelativeLayout) findViewById(R.id.layout);
        barchart = new BarchartView(this);
        layout.addView(barchart);

    }

    /**
    * onActivityResult-Methode
    * @param requestCode wird zur Aktionsunterscheidung als Integer übergeben
    * @param resultCode
    * @param data
    */
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        onCreateInhalt();
    }

    /**
     * Methode zum Aufrufen eines Pop-Ups um eine Aufgabe zu löschen
    * @param pos Position des ausgewählten Listview-Items
    */
    private void loeschenDialog(int pos) {
        final int posi = pos;
        //neues Pop-Up erstellen
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        //angezeigter Text im Pop-Up
        builder.setTitle(getString(R.string.title_activity_aufgabe) + " '" + bezeichnungen.get(pos) + "' " + getString(R.string.str_wirklich_löschen) + "?");
        //Button zum Löschen der Aufgabe im Pop-Up erstellen
        builder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                datenbank.deleteAufgabe(ids.get(posi));
                onCreateInhalt();
                dialog.dismiss();
            }
        });
        //Button zum Abbrechen des Löschvorgangs erstellen
        builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // Löschen wird abgebrochen
                dialog.dismiss();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    /**
     * onItemSelcted-Methode für das Statusfilter-Drop-Down-Menü
     * @param parent AdapterView
     * @param view View-Objekt des ausgewählten Drop-Down-Menü-Elements
     * @param position  Position des ausgewählten Drop-Down-Menü-Elements
     * @param id ID des ausgewählten Drop-Down-Menü-Elements
     */
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        //sobald ein neuer Filter ausgewählt wird, wird der Inhalt der ListView aktualisiert
        onCreateInhalt();
    }

    /**
     * onNothingSelectes-Methode für das Statusfilter-Drop-Down-Menü
     * @param parent AdapterView
     */
    public void onNothingSelected(AdapterView<?> parent) {
        onCreateInhalt();
    }
}
