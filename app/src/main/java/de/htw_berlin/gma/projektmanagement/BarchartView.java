package de.htw_berlin.gma.projektmanagement;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

/**
 * View-Klasse zum Malen des Balken-Diagramms
 * @author Delia
 * @author Igor
 */
public class BarchartView extends View {

    //Konstanten zum Rechteck malen
    private int top;
    private int bottom;
    private int textheight;



    DB datenbank;

    private Paint paint = new Paint();
    private Paint textpaint = new Paint();
    private String[] status = new String[3];
    private int offen;
    private int inBearbeitung;
    private int abgeschlossen;

    public BarchartView(Context context) {
        super(context);
    }

    public BarchartView(Context context, AttributeSet attrs){
        super(context, attrs);
    }

    public BarchartView(Context context, AttributeSet attrs, int defStyle){
        super(context, attrs, defStyle);
    }

    /**
     * Methode zum Malen des Balkendiagramms
     * @param canvas Leinwand
     */
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        datenbank = new DB(getContext());
        //Status aus der Datenbank erfragen
        status = datenbank.queryStatus();

        // Höhe der Rechtecke bestimmen
        // um sie an verschiedene Bildschirmauflösungen anzupassen
        int[] loc = new int[2];
        getLocationOnScreen(loc);
        bottom = loc[1] - (int)(loc[1] * 0.2);
        top = loc[0] + (int)(loc[0] * 1.5);
        textheight = (int)(bottom * 0.85);

        //Array_Felder in Integer umwandeln und den int-Variablen zuweisen
        offen = Integer.parseInt(status[0]);
        inBearbeitung = Integer.parseInt(status[1]);
        abgeschlossen = Integer.parseInt(status[2]);
        //Gesamtzahl aller Aufgaben ermitteln
        int gesamt = offen + inBearbeitung + abgeschlossen;

        //Farbe und Größe für die Schrift im Diagramm festlegen
        textpaint.setColor(Color.WHITE);
        textpaint.setTextSize(40);

        //Variablen um den Anfang und das Ende der Balkenabschnitte zu ermitteln
        int start = 0;
        int ende;

        //wenn der int-Wert 0 sein sollte, ist keine Aufgabe mit dem entsprechenden Status vorhanden
        //demnach muss kein Balken der zugehörigen Farbe erstellt werden
        if(offen > 0) {
            //Längenberechnung fuer den Balken
            ende = (getWidth()*((offen*100)/gesamt))/100;
            //Farbe des Balkens festlegen
            paint.setColor(Color.parseColor(getResources().getString(R.color.colorAccent)));
            //Rechteck/Balken malen
            canvas.drawRect(start, top, getWidth(), bottom, paint);
            //Text in die Mitte des Rechtecks schreiben
            canvas.drawText(status[0], ende / 2 + start, textheight, textpaint);
            //start-Variable für den nächsten Balken erhöhene
            start = start + ende;
        }

        if(inBearbeitung > 0) {
            ende = (getWidth()*((inBearbeitung*100)/gesamt))/100;
            paint.setColor(Color.parseColor(getResources().getString(R.color.color_in_Bearbeitung)));
            canvas.drawRect((float) start, top, getWidth(), bottom, paint);
            canvas.drawText(status[1], ende / 2 + start, textheight, textpaint);
            start = start + ende;
        }

        if(abgeschlossen > 0) {
            ende = (getWidth() * ((abgeschlossen*100)/gesamt))/100;
            paint.setColor(Color.parseColor(getResources().getString(R.color.color_Abgeschlossen)));
            canvas.drawRect(start, top, getWidth(), bottom, paint);
            canvas.drawText(status[2], ende/2 + start, textheight, textpaint);
        }
    }
}
