package de.htw_berlin.gma.projektmanagement;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

/**
 * Klasse zur Darstellung der Aufgaben-Informationen
 * zugehörige Activity: activity_aufgabe.xml
 * @author Delia Metzler
 * @author Igor Turanin
 */
public class Aufgabe extends AppCompatActivity {

    //Reguest-Codes
    final static int NEUE_AUFGABE = -1;
    final static int BEARBEITEN_REQUESTCODE = 11;

    //
    final static String STATUS_OFFEN = "0";
    final static String STATUS_IN_BEARBEITUNG = "1";
    final static String STATUS_ABGESCHLOSSEN = "2";

    // ermöglicht nach späterer Initialisierung den Zugriff auf die lokale Datenbank der App
    DB datenbank;

    // ermöglichen nach späterer Initialisierung den Zugriff auf die TextViews
    TextView bezeichnung;
    TextView beschreibung;
    TextView deadline;
    TextView bearbeiter;
    TextView status;

    private int aID = NEUE_AUFGABE;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_aufgabe);

        //Zurück-Pfeil auf dem Actionbar erzeugen
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // neues Objekt der Klasse DB wird erstellt um den Zugriff auf die lokale Datenbank zu gewährleisten
        datenbank = new DB(getApplicationContext());

        //Textfelder in der View finden
        bezeichnung = (TextView) findViewById(R.id.tv_aufgabenbezeichnung);
        beschreibung = (TextView) findViewById(R.id.tv_beschreibung_info);
        deadline = (TextView) findViewById(R.id.tv_datum);
        bearbeiter = (TextView) findViewById(R.id.tv_bearbeiter_info);
        status = (TextView) findViewById(R.id.tv_status);

        //Initialisierung der Aufgaben-ID
        aID = getIntent().getIntExtra("a_id",NEUE_AUFGABE);

        //Inhalt der Activity erzeugen
        onCreateInhalt();

    }

    /**
     * onClick-Methode für den Bearbeiten-Button
     * @param v View-Objekt des betätigten Buttons
     */
    public void onButtonClick(View v) {
        if (v.getId() == R.id.btn_bearbeiten) {
            //beim Klick auf den Button wird eine neue Activity gestartet
            Intent intent = new Intent(this, AufgabeBearbeiten.class);
            //zusätzlich wird die Aufgaben-ID übergeben
            intent.putExtra("a_id", aID);
            //durch Übergabe des Requestcodes weiß die nächste Activity welche Aktionen sie ausführen soll
            startActivityForResult(intent,BEARBEITEN_REQUESTCODE);
        }
    }

    /**
     * onActivityResult-Methode
     * @param requestCode wird zur Aktionsunterscheidung als Integer übergeben
     * @param resultCode
     * @param data
     */
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == BEARBEITEN_REQUESTCODE && resultCode == Activity.RESULT_OK) {
            onCreateInhalt();
        }
    }

    /**
     * Inhalte der Activity erzeugen
     */
    private void onCreateInhalt() {

        //Aufgaben-Informationen als String-Array aus der Datenbank abfragen
        String[] infos = datenbank.queryAufgabe(getIntent().getExtras().getInt("a_id"));

        //TextViews mit Inhalt füllen
        bezeichnung.setText(infos[1]);      //Aufgabenbezeichnung
        beschreibung.setText(infos[2]);     //Aufgabenbeschreibung
        deadline.setText(infos[3]);         //Deadline
        bearbeiter.setText(infos[4]);       //Bearbeiter

        // setzt den richtigen Status als Text ein
        if (infos[5].equals(STATUS_OFFEN)) {
            status.setText(getString(R.string.str_offen));
            status.setBackgroundColor(Color.parseColor(getString(R.color.colorAccent)));
        } else if(infos[5].equals(STATUS_IN_BEARBEITUNG)) {
            status.setText(getString(R.string.str_in_bearbeitung));
            status.setBackgroundColor(Color.parseColor(getString(R.color.color_in_Bearbeitung)));
        } else if(infos[5].equals(STATUS_ABGESCHLOSSEN)) {
            status.setText(getString(R.string.str_abgeschlossen));
            status.setBackgroundColor(Color.parseColor(getString(R.color.color_Abgeschlossen)));
        } else {
            status.setText(getString(R.string.str_status_default));
        }
    }

    /**
     * onOptionsItemSelected-Methode für den Zurück-Button auf dem Action-Bar
     * @param item entsprechendes Menü-Item
     * @return boolean Ausführen
     */
    public boolean onOptionsItemSelected(MenuItem item) {
        //zur vorherigen Activity wechseln
        onBackPressed();
        return true;
    }
}
